﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.Azure; // Namespace for CloudConfigurationManager
using Microsoft.Azure.Storage; // Namespace for StorageAccounts
using Microsoft.Azure.CosmosDB.Table;
using LuisBot.Models;
using System.Threading.Tasks;

namespace LuisBot
{
    public class AzureStorageService
    {
        protected CloudTableClient tableClient;

        public AzureStorageService()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            this.tableClient = storageAccount.CreateCloudTableClient();
        }

        public async Task<DirectionEntity> RetrieveDirection(string name)
        {
            CloudTable directionsTable = tableClient.GetTableReference("directionstable");
            name = name.Replace(" ", "");
            TableOperation retrieveOperation = TableOperation.Retrieve<DirectionEntity>("directions", name);
            var result = await directionsTable.ExecuteAsync(retrieveOperation);
            if (result.Result != null)
                return (DirectionEntity)result.Result;
            return null;
        }
        
    }
}