﻿using LuisBot.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace LuisBot
{
    public class AzureSearchService
    {
        public async Task<DirectionEntity> RetrieveDirection(string name)
        {
            string url = $"https://malldesksearch.search.windows.net/indexes/malldeskindex/docs?api-version=2017-11-11&queryType=full&search={name}~5";

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("api-key", "05D5B177CD815B81C7AA4653145BB52E");
                HttpResponseMessage response = await client.GetAsync(url);
                DirectionsList list = JsonConvert.DeserializeObject<DirectionsList>(response.Content.ReadAsStringAsync().Result);
                if(list.value.Count != 0)
                {
                    Direction direction = list.value[0];
                    return new DirectionEntity()
                    {
                        name = direction.name,
                        descriptions = direction.descriptions
                    };
                }
                return null;
            }
        }

        public async Task<LostItemEntity> RetrieveLostItem(string name, string brand, string color, string descriptions)
        {
            name = name.Replace(" ", "");
            brand = brand.Replace(" ", "");
            color = color.Replace(" ", "");
            string url = $"https://malldesksearch.search.windows.net/indexes/lostandfoundindex/docs?api-version=2017-11-11&search={name}%20{brand}%20{color}%20{descriptions}";
            url = url.Replace(" ", "%20");

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("api-key", ConfigurationManager.AppSettings["api-key"]);
                HttpResponseMessage response = await client.GetAsync(url);
                ItemList list = JsonConvert.DeserializeObject<ItemList>(response.Content.ReadAsStringAsync().Result);
                JObject jresponse = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                JArray jresponses = (JArray)jresponse["value"];

                if (jresponses.Count != 0)
                {
                    JObject item = (JObject)jresponses.First;
                    double score = double.Parse(item["@search.score"].ToString());
                    bool isReleased = bool.Parse(item["released"].ToString());
                    if (score >= .50)
                    {
                        return new LostItemEntity(
                            item["name"].ToString(),
                            item["brand"].ToString(),
                            item["color"].ToString(),
                            item["descriptions"].ToString())
                        {
                            Score = score >= 1 ? .98 : score,
                            FoundBy = item["foundby"].ToString(),
                            DateFound = new DateTimeOffset(Convert.ToDateTime(item["founddate"])),
                            IsReleased = isReleased,
                            FoundPlace = item["foundplace"].ToString()
                        };
                    }
                }
                return null;
            }
        }
    }

    public class Direction
    {
        public string key { get; set; }
        public string name { get; set; }
        public object direction { get; set; }
        public string descriptions { get; set; }
    }

    public class DirectionsList
    {
        public List<Direction> value { get; set; }
    }

    public class Item
    {
        public string Name { get; set; }
        public string Brand { get; set; }
        public string Color { get; set; }
        public string Descriptions { get; set; }
    }

    public class ItemList
    {
        public List<Item> Value { get; set; }
    }
}