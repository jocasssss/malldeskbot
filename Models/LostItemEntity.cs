﻿using Microsoft.Azure.CosmosDB.Table;
using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LuisBot.Models
{
    [Serializable]
    public class LostItemEntity : TableEntity
    {
        public string Name { get; set; }
        public string Brand { get; set; }
        public string Color { get; set; }
        public string Description { get; set; }
        public string DescriptionString { get; set; }
        public double Score { get; set; }
        public string FoundBy { get; set; }
        public DateTimeOffset DateFound { get; set; }
        public bool IsReleased { get; set; }
        public string FoundPlace { get; set; }

        public LostItemEntity(string name, string brand, string color, List<string> descriptionList)
        {
            this.PartitionKey = "lostitem";
            this.RowKey = name + brand + color;
            this.Name = name;
            this.Brand = brand;
            this.Color = color;

            foreach(var item in descriptionList)
            {
                this.Description += item + ",";
            }
        }

        public LostItemEntity(string name, string brand, string color, string descriptionString)
        {
            this.PartitionKey = "lostitem";
            this.RowKey = name + brand + color;
            this.Name = name;
            this.Brand = brand;
            this.Color = color;
            this.DescriptionString = descriptionString;
        }

    }
}