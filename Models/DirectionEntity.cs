﻿using Microsoft.Azure.CosmosDB.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LuisBot.Models
{
    [Serializable]
    public class DirectionEntity : TableEntity
    {
        public string name { get; set; }
        public string descriptions { get; set; }

        public DirectionEntity(string name, List<string> descriptions)
        {
            this.name = name;
            this.PartitionKey = "directions";
            this.RowKey = this.name;

            string descriptionString = "";
            foreach(var item in descriptions)
            {
                descriptionString += item + ",";
            }
            this.descriptions = descriptionString.Remove(descriptionString.Length - 1);
        }

        public DirectionEntity()
        {

        }
    }
}