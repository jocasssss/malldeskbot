using System;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LuisBot;
using LuisBot.Dialogs;
using LuisBot.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using Newtonsoft.Json.Linq;

namespace Microsoft.Bot.Sample.LuisBot
{
    [LuisModel("5815fce7-3a74-48cd-8459-c3909a3b167d", "115b6a2886c145ec87419b7da1f32abf")]
    [Serializable]
    public class BasicLuisDialog : LuisDialog<object>
    {

        [LuisIntent("Directions")]
        public async Task DirectionsIntent(IDialogContext context, LuisResult result)
        {
            if (result.Entities.Count != 0)
            {
                string directionName = result.Entities[0].Entity;
                var activity = context.MakeMessage();
                activity.Text = directionName;
                await context.Forward<object>(new DirectionsDialog(), DirectionsResumeAfter, activity);
            }
            else
            {
                await None(context, result);
                return;
            }
        }

        private async Task DirectionsResumeAfter(IDialogContext context, IAwaitable<object> result)
        {
            context.Wait(MessageReceived);
        }

        [LuisIntent("LostItem")]
        public async Task LostItemIntentAsync(IDialogContext context, LuisResult result)
        {
            if(result.Entities.Count == 0)
            {
                await None(context, result);
            }
            else
            {
                context.Activity.AsMessageActivity().Text = result.Entities[0].Entity;
                await context.Forward(new LostItemDialogTest(), LostItemResumeAfter, context.Activity.AsMessageActivity());
            }
        }

        private async Task LostItemResumeAfter(IDialogContext context, IAwaitable<LostItemEntityForm> result)
        {
            LostItemEntityForm lostItemForm = await result;
            AzureSearchService searchService = new AzureSearchService();
            LostItemEntity lostItem = await searchService.RetrieveLostItem(lostItemForm.Name, lostItemForm.Brand, lostItemForm.Color, lostItemForm.Description);

            var response = context.MakeMessage();
            if (lostItem != null)
            {
                var foundResponse = context.MakeMessage();
                int rate = (int)(lostItem.Score * 100);
                foundResponse.Text = $"I found a {lostItem.Color} {lostItem.Brand} {lostItem.Name} in our lost and found. It was found {lostItem.DateFound.DateTime.ToString("MMMM dd")} around {lostItem.DateFound.DateTime.ToString("h:mm tt")} {lostItem.FoundPlace}. Based on your description, it's {rate} percent likely that it is yours. You can claim it at our lost and found, 3rd floor, beside the food court. Have a good shopping!";
                await context.PostAsync(foundResponse);
            }
            else
            {
                response.Text = "Sorry, I couldn't find what you are looking for. Try visiting our lost and found at 3rd floor, beside the food court";
                await context.PostAsync(response);
            }

            context.Wait(MessageReceived);
        }

        [LuisIntent("FoundItem")]
        public async Task FoundItemIntentAsync(IDialogContext context, LuisResult result)
        {
            if(result.Entities.Count == 0)
            {
                await None(context, result);
            }
            else
            {
                context.Activity.AsMessageActivity().Text = result.Entities[0].Entity;
                await context.Forward(new FoundItemDialog(), FoundItemResumeAfter, context.Activity.AsMessageActivity());
            }
        }

        public async Task FoundItemResumeAfter(IDialogContext context, IAwaitable<FoundItemForm> resultAwaitable)
        {
            FoundItemForm foundItem = await resultAwaitable;
            var response = context.MakeMessage();
            response.Text = "Thank you for letting us know. You can surrender the lost item to our lost and found, third floor, beside the food court";

            using (HttpClient client = new HttpClient())
            {
                JObject item = new JObject(
                    new JProperty("name", foundItem.Name),
                    new JProperty("brand", foundItem.Brand),
                    new JProperty("color", foundItem.Color),
                    new JProperty("descriptions", foundItem.Description),
                    new JProperty("datefound", DateTime.Now),
                    new JProperty("foundplace", foundItem.FoundPlace));

                HttpRequestMessage req = new HttpRequestMessage()
                {
                    RequestUri = new Uri("https://prod-11.southeastasia.logic.azure.com:443/workflows/d06b16fd18c64f58a7ac44b01ca2d6e8/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=WdYOymBh8cxl60cI5s2dmRN6nBbRKl4YwBMFlRsFZpg"),
                    Method = HttpMethod.Post,
                    Content = new StringContent(item.ToString(), Encoding.UTF8, "application/json"),
                };
                HttpResponseMessage resp = client.SendAsync(req).Result;
                string respString = await resp.Content.ReadAsStringAsync();
            }

            await context.PostAsync(response);
        }

        protected override async Task MessageReceived(IDialogContext context, IAwaitable<IMessageActivity> item)
        {
            var message = await item;
            if (message.Text == null)
            {
                await Help(context, null);
            }
            else
            {
                await base.MessageReceived(context, item);
            }
        }

        [LuisIntent("Help")]
        public async Task Help(IDialogContext context, LuisResult result)
        {
            var response = context.MakeMessage();
            response.Summary = "Welcome to the Information Desk";
            response.Speak = "Welcome to the information desk. You can ask me for directions and lost items. What can I do for you?";
            response.InputHint = InputHints.ExpectingInput;
            await context.PostAsync(response);
            context.Wait(MessageReceived);
        }
        
        [LuisIntent("None")]
        public async Task None(IDialogContext context, LuisResult result)
        {
            var response = context.MakeMessage();
            response.Text = "Sorry. I don't know that. You can say something like 'Where is Mcdonalds?' or 'I lost my wallet'~";
            response.InputHint = InputHints.ExpectingInput;
            await context.PostAsync(response);
            context.Wait(MessageReceived);
        }
    }
}