﻿using LuisBot.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LuisBot.Dialogs
{
    [Serializable]
    public class DirectionsDialog : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageRecievedAsync);
        }

        private async Task MessageRecievedAsync(IDialogContext context, IAwaitable<IMessageActivity> activityAwaitble)
        {
            IMessageActivity activity = await activityAwaitble;
            string directionName = activity.Text;
            await RespondChecking(context, directionName);
            DirectionEntity direction = await GetDirection(directionName);
            if(direction != null)
            {
                await RespondDirection(context, direction);
            }
            else
            {
                await RespondNotFound(context, directionName);
            }
            await Task.Delay(3000);
            //await PromptFindAnother(context);
        }

        private async Task RespondChecking(IDialogContext context, string directionName)
        {
            directionName = directionName.Replace(" ' ", "'");
            var responseString = $"Let me check {directionName} from my directory";
            var response = context.MakeMessage();
            response.Text = responseString;
            response.Summary = responseString;
            response.Speak = responseString;
            await context.PostAsync(response);
        }

        private async Task RespondDirection(IDialogContext context, DirectionEntity direction)
        {
            var responseString = $"You can find {direction.name} at {direction.descriptions}. Have a good shopping!";
            var response = context.MakeMessage();
            response.Text = responseString;
            response.Summary = responseString;
            response.Speak = responseString;
            await context.PostAsync(response);
        }

        private async Task RespondNotFound(IDialogContext context, string directionName)
        {
            var responseString = $"I can't find {directionName} under my directory";
            var response = context.MakeMessage();
            response.Text = responseString;
            response.Summary = responseString;
            response.Speak = responseString;
            await context.PostAsync(response);
        }

        private async Task AskDirectionInquiry(IDialogContext context)
        {
            var responseString = "What are you looking for?";
            var response = context.MakeMessage();
            response.Text = responseString;
            response.Summary = responseString;
            response.Speak = responseString;
            await context.PostAsync(response);
        }

        private async Task PromptFindAnother(IDialogContext context)
        {
            string promptString = $"Do you want to look for another place?";
            PromptDialog.Confirm(
                context,
                FindAnotherResumeAfter,
                new PromptOptions<string>(
                    prompt: promptString,
                    speak: promptString,
                    options: new List<string>()
                    {
                        "Yes","No"
                    }));
        }

        private async Task FindAnotherResumeAfter(IDialogContext context, IAwaitable<bool> resultAwaitable)
        {
            bool yes = await resultAwaitable;
            if (yes)
            {
                await AskDirectionInquiry(context);
                context.Wait(MessageRecievedAsync);
            }
            else
            {
                await context.SayAsync(speak: "Okay", text: "Okay");
                context.Done(new Object());
            }
        }

        private async Task<DirectionEntity>GetDirection(string directionName)
        {
            AzureSearchService searchService = new AzureSearchService();
            DirectionEntity direction = await searchService.RetrieveDirection(directionName);
            return direction;
        }
    }
}