﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Dialogs;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.FormFlow;

namespace LuisBot.Dialogs
{
    [Serializable]
    public class LostItemDialogTest : IDialog<LostItemEntityForm>
    {
        private string itemName;

        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageRecievedAsync);
        }

        private async Task MessageRecievedAsync(IDialogContext context, IAwaitable<IMessageActivity> activityAwaitable)
        {
            IMessageActivity activity = await activityAwaitable;
            itemName = activity.Text;
            var LostItemEntityFormDialog = FormDialog.FromForm(this.BuildForm, FormOptions.PromptInStart);
            context.Call(LostItemEntityFormDialog, LostItemEntityFormDialogResumeAfter);
        }

        private async Task LostItemEntityFormDialogResumeAfter(IDialogContext context, IAwaitable<LostItemEntityForm> resultAwaitable)
        {
            LostItemEntityForm lostItem = await resultAwaitable;
            lostItem.Name = itemName;
            context.Done(lostItem);
        }

        private IForm<LostItemEntityForm> BuildForm()
        {
            return new FormBuilder<LostItemEntityForm>()
                .Field("Brand",$"What is the brand of your {itemName}?")
                .Field("Color",$"What is the color of your {itemName}?")
                .Field("Description",$"Please give me one more description of your missing item~")
                .Confirm(async (state) =>
                    {
                        return new PromptAttribute($"You are looking for a {state.Color} {state.Brand} {itemName}. {state.Description}. Correct?");
                    }
                )
                .Build();
        }
    }

    [Serializable]
    public class LostItemEntityForm
    {
        public string Brand { get; set; }
        public string Color { get; set; }
        public string Description { get; set; }
        public string Name;
    }
}