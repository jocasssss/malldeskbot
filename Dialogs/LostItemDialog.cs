﻿using LuisBot.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LuisBot.Dialogs
{
    [Serializable]
    public class LostItemDialog : IDialog<string>
    {
        private int step = 0;
        private string name;
        private string brand;
        private string color;
        private List<string> descriptions = new List<string>();

        public async Task StartAsync(IDialogContext context)
        {
            //await GetDescriptionsAsync(context);
            context.Wait(MessageRecievedAsync);
        }

        public async Task MessageRecievedAsync(IDialogContext context, IAwaitable<IMessageActivity> activityAwaitable)
        {

            IMessageActivity activity = await activityAwaitable;
            string message = activity.Text;
            var response = context.MakeMessage();

            switch (step)
            {
                case 0:
                    {
                        response.Text = "I'm going to need some description of your missing item";
                        response.Summary = "I'm going to need some description of your missing item";
                        response.Speak = "I'm going to need some description of your missing item";
                        response.InputHint = InputHints.IgnoringInput;
                        await context.PostAsync(response);
                        await Task.Delay(2000);

                        step++;

                        if (context.Activity.AsMessageActivity().Text != null)
                        {
                            this.name = context.Activity.AsMessageActivity().Text;
                            step = 2;
                        }
                        await GetDescriptionsAsync(context);
                        break;
                    }
                case 1:
                    {
                        this.name = message;
                        step++;
                        await GetDescriptionsAsync(context);
                        break;
                    }
                case 2:
                    {
                        this.brand = message;
                        step++;
                        await GetDescriptionsAsync(context);
                        break;
                    }
                case 3:
                    {
                        this.color = message;
                        step++;
                        await GetDescriptionsAsync(context);
                        break;
                    }
                case 4:
                    {
                        bool yes = message.ToLower().Equals("yes");
                        if (yes)
                        {
                            step++;
                        }
                        else
                        {
                            step = 6;
                        }
                        await GetDescriptionsAsync(context);
                        break;
                    }
                case 5:
                    {
                        this.descriptions.Add(message);
                        step++;
                        await GetDescriptionsAsync(context);
                        break;
                    }
                case 6:
                    {
                        bool yes = message.ToLower().Contains("yes");
                        if (yes)
                        {
                            response.Text = "Let me check that out on our lost and found";
                            response.Summary = "Let me check that out on our lost and found";
                            response.Speak = "Let me check that out on our lost and found";
                            response.InputHint = InputHints.IgnoringInput;
                            await context.PostAsync(response);
                            step++;

                            string descriptionString = "";
                            foreach (string item in descriptions) descriptionString += item + " ";
                            string searchString = $"{this.name} {this.brand} {this.color} {descriptionString}";

                            context.Done(searchString);
                        }
                        else
                        {
                            await context.SayAsync(
                                speak: "I'm sorry, let's try that again",
                                text: "I'm sorry, let's try that again");
                            step = -1;
                            context.Wait(MessageRecievedAsync);
                        }
                        break;
                    }
            }
        }

        public async Task GetDescriptionsAsync(IDialogContext context)
        {
            var response = context.MakeMessage();
            switch (step)
            {
                case 1:
                    {
                        response.Text = "What item are you missing again?";
                        response.Summary = "What item are you missing again?";
                        response.Speak = "What item are you missing?";
                        response.InputHint = InputHints.ExpectingInput;
                        await context.PostAsync(response);
                        break;
                    }
                case 2:
                    {
                        response.Text = $"What is the brand of your {this.name}?";
                        response.Summary = $"What is the brand of your {this.name}?";
                        response.Speak = $"What is the brand of your {this.name}?";
                        response.InputHint = InputHints.ExpectingInput;
                        await context.PostAsync(response);
                        break;
                    }
                case 3:
                    {
                        response.Text = $"What is the color of your {name}?";
                        response.Summary = $"What is the color of your {name}?";
                        response.Speak = $"What is the color of your {name}?";
                        response.InputHint = InputHints.ExpectingInput;
                        await context.PostAsync(response);
                        break;
                    }
                case 4:
                    {
                        response.Text = $"Do you have other description of your missing item?";
                        response.Summary = $"Do you have other description of your missing item?";
                        response.Speak = $"Do you have other description of your missing item?";
                        response.InputHint = InputHints.ExpectingInput;
                        await context.PostAsync(response);
                        break;
                    }
                case 5:
                    {
                        response.Text = "What other description do you have?";
                        response.Summary = "What other description do you have?";
                        response.Speak = "What other description do you have?";
                        await context.PostAsync(response);
                        break;
                    }
                case 6:
                    {
                        string desc = "";
                        foreach (var item in descriptions) desc += item + ",";
                        response.Text = $"You are looking for a {this.color} {this.brand} {this.name}. {desc}. Is that correct?";
                        response.Summary = $"You are looking for a {this.color} {this.brand} {this.name}. {desc}. Is that correct?";
                        response.Speak = $"You are looking for a {this.color} {this.brand} {this.name}. {desc}. Is that correct?";
                        response.InputHint = InputHints.ExpectingInput;
                        await context.PostAsync(response);
                        break;
                    }
            }
            context.Wait(MessageRecievedAsync);
        }


    }
}