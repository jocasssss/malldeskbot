﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;

namespace LuisBot.Dialogs
{
    [Serializable]
    public class FoundItemDialog : IDialog<FoundItemForm>
    {
        private string itemName;
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageRecievedAsync);
        }

        private async Task MessageRecievedAsync(IDialogContext context, IAwaitable<IMessageActivity> activityAwaitable)
        {
            IMessageActivity message = await activityAwaitable;
            itemName = message.Text;
            var foundItemFormDialog = FormDialog.FromForm(this.BuildForm, FormOptions.PromptInStart);
            context.Call(foundItemFormDialog, FoundItemFormResumeAfter);
        }

        private async Task FoundItemFormResumeAfter(IDialogContext context, IAwaitable<FoundItemForm> resultAwaitble)
        {
            FoundItemForm foundItemForm = await resultAwaitble;
            foundItemForm.Name = itemName;
            context.Done(foundItemForm);
        }

        private IForm<FoundItemForm> BuildForm()
        {
            return new FormBuilder<FoundItemForm>()
                .Field("Brand", $"What is the brand of that {itemName}?")
                .Field("Color", $"What is the color of that {itemName}?")
                .Field("Description", $"Please give me one more description of that lost {itemName}~")
                .Field("FoundPlace", $"Where did you found it?")
                .Confirm(async (state) =>
                    {
                        return new PromptAttribute($"You found a {state.Brand} {state.Color} {itemName} {state.FoundPlace}. Correct?");
                    }
                )
                .Build();
        }
    }

    [Serializable]
    public class FoundItemForm
    {
        public string Brand { get; set; }
        public string Color { get; set; }
        public string Description { get; set; }
        public string FoundPlace { get; set; }
        public string Name; 
    }
}