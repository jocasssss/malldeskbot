﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LuisBot
{
    public class TextToSpeechMapper : IMessageActivityMapper
    {
        public IMessageActivity Map(IMessageActivity message)
        {
            var channelCapability = new ChannelCapability(Address.FromActivity(message));
            if(channelCapability.SupportsSpeak() && string.IsNullOrEmpty(message.Speak))
            {
                if (message.Text.EndsWith("?"))
                {
                    message.InputHint = InputHints.ExpectingInput;
                }
                else if (message.Text.EndsWith("~"))
                {
                    message.Text = message.Text.Replace("~", "");
                    message.InputHint = InputHints.ExpectingInput;
                }
                message.Speak = message.Text;
            }
            return message;
        }
    }
}